# KIBANA DASHBOARD ON DOCKER

Kibana is a dashboard for data search and visualisation from Elasticsearch.


## How to Install

Using the [Bash Package Manager](https://gitlab.com/exadra37-bash/package-manager) just go to the folder where you want it 
installed and type:

```
$ bpm require exadra37-docker/data-dashboard kibana last-stable-release gitlab.com
```
